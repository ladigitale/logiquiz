export default (
    integration,
    scriptsBundle,
    stylesBundle,
    contentId
) => `
<!DOCTYPE html>
<html class="h5p-iframe">
    <head>
        <meta charset="utf-8">                    
        <script>H5PIntegration = ${JSON.stringify({
            ...integration,
            baseUrl: '.',
            url: '.',
            ajax: { setFinished: '', contentUserData: '' },
            saveFreq: false,
            libraryUrl: ''
        })};
        ${scriptsBundle}</script>
        <style>html.h5p-iframe,html.h5p-iframe>body{height:calc(100% - 40px)!important;}${stylesBundle}</style>
    </head>
    <body>
        <div style="display: flex; justify-content: center; margin: 20px;">
            <div class="h5p-content lag" data-content-id="${contentId}" style="max-width: 1400px;"></div>
        </div>            
    </body>
</html>`
