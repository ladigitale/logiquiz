# Logiquiz

Logiquiz est un logiciel pour lire et créer des contenus H5P hors ligne.

Il est publié sous licence GNU GPLv3.
Sauf les fontes Roboto, Roboto Slab, Material Icons (Apache License Version 2.0), la fonte HKGrotesk (Sil Open Font Licence 1.1) et FileSaver.j - https://github.com/eligrey/FileSaver.js/ - (MIT)

## Préparation et installation des dépendances
```
npm install
```

### Démarrage du logiciel
```
npm run start
```

### Compilation du logiciel
```
pour Windows : npm run dist:win
pour macOS : npm run dist:mac (fichier provisionprofile nécessaire)
pour GNU/Linux : npm run dist:linux
```

### Présentation
https://ladigitale.dev/logiquiz/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

