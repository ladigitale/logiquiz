class User {
    constructor() {
        this.id = '1'
        this.name = 'Logiquiz'
        this.email = 'ez@ladigitale.dev'
        this.canInstallRecommended = true
        this.canUpdateAndInstallLibraries = true
        this.canCreateRestricted = true
        this.type = 'local'
    }
}

export default User
