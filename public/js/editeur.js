var ns = H5PEditor;
var contentId = 'undefined';

(function($) {
    H5PEditor.init = function() {
		// ns.Editor.prototype.semiFullscreen = undefined;
		H5PEditor.$ = H5P.jQuery;
		H5PEditor.contentLanguage = 'fr';
        H5PEditor.basePath = H5PIntegration.editor.libraryUrl;
        H5PEditor.fileIcon = H5PIntegration.editor.fileIcon;
        H5PEditor.ajaxPath = H5PIntegration.editor.ajaxPath;
        H5PEditor.filesPath = H5PIntegration.editor.filesPath;
        H5PEditor.apiVersion = H5PIntegration.editor.apiVersion;
        H5PEditor.copyrightSemantics = H5PIntegration.editor.copyrightSemantics;
        H5PEditor.metadataSemantics = H5PIntegration.editor.metadataSemantics;
        H5PEditor.assets = H5PIntegration.editor.assets;
		H5PEditor.baseUrl = '';

        if (H5PIntegration.editor.nodeVersionId !== undefined) {
			H5PEditor.contentId = H5PIntegration.editor.nodeVersionId;
			contentId = H5PIntegration.editor.nodeVersionId;
		}

        var h5peditor;
		var $iframe;
        var $creer = $('.h5p-create');
        var $editeur = $('.h5p-editor');
		var $lire = $('.h5p-read');
        var $lecteur = $('.h5p-player');
		var $enregistrer = $('#h5p-save');
		var $afficher = $('#h5p-preview');
		var $editer = $('#h5p-edit');
		var $modaleAlerte = $('#modale-alerte');
		var $loader = $('#loader');

		$creer.hide();
		$editer.hide();
		$afficher.hide();
		$enregistrer.hide();

        if (h5peditor === undefined) {
			if (H5PEditor.contentId) {
				$.ajax({
					error: function () {
						h5peditor = new ns.Editor(undefined, undefined, $editeur[0]);
						$creer.fadeIn();
						chargerContenu();
					},
					success: function (res) {
						h5peditor = new ns.Editor(
							res.library,
							JSON.stringify(res.params),
							$editeur[0]
						);
						$creer.fadeIn();
						chargerContenu();
					},
					type: 'GET',
					url: '/h5p/params/' + H5PEditor.contentId + window.location.search
				});
			} else {
				h5peditor = new ns.Editor(undefined, undefined, $editeur[0]);
				$creer.fadeIn();
				chargerContenu();
			}
        }

		$('body').on('keydown', function (event) {
			if (event.key === 'Escape' && $modaleAlerte.hasClass('ouvert')) {
				$('#valider').click();
			}
		});
		
		$afficher.click(function () {
			var $titre = $('.h5p-editor-iframe').contents().find('.field-name-extraTitle input').val();
			if ($titre !== '') {
				$('body').scrollTop(20);
				enregistrerContenu(true);
			} else {
				$modaleAlerte.find('.message').text('Veuillez remplir le champ Titre.');
				$modaleAlerte.addClass('ouvert');
			}
        });
		
		$editer.click(function () {
			$('body').scrollTop(20);
			$('body').addClass('editeur');
			$('body').removeClass('lecteur');
			$lecteur.html('');
			$lire.hide();
			$editer.hide();
			$afficher.show();
			$creer.show();
        });
        
        $enregistrer.click(function () {
			var $titre = $('.h5p-editor-iframe').contents().find('.field-name-extraTitle input').val();
			if ($titre !== '') {
            	enregistrerContenu(false);
			} else {
				$modaleAlerte.find('.message').text('Veuillez remplir le champ Titre.');
				$modaleAlerte.addClass('ouvert');
			}
		});

		$('#valider').click(function () {
			$modaleAlerte.removeClass('ouvert');
			$modaleAlerte.find('.message').text('');
		});

		function chargerContenu () {
			$iframe = $('.h5p-editor-iframe');
			$iframe.on('load', function () {
				setTimeout(function () {
					$loader.hide();
				}, 500)
				var $head = $iframe.contents().find('head');
				$head.append('<style>html,body{overflow:hidden!important;width:100%!important;max-width:100%!important;}.h5peditor{padding:2rem;margin:auto;max-width:960px;}.h5peditor-form-manager-fullscreen{display:none;}</style>');
				$afficher.show().addClass('desactive');
				$enregistrer.show().addClass('desactive');

				$iframe.contents().find('body').on('click', function () {
					setTimeout(function () {
						var titre = $('.h5p-editor-iframe').contents().find('.field-name-extraTitle input').val();
						var $formulaire = $('.h5p-editor-iframe').contents().find('.h5peditor-form');
						if ($formulaire.length > 0) {
							$afficher.show();
							$enregistrer.show();
						} else {
							$afficher.hide();
							$enregistrer.hide();
						}
						if (titre !== '') {
							$afficher.removeClass('desactive');
							$enregistrer.removeClass('desactive');
						} else {
							$afficher.addClass('desactive');
							$enregistrer.addClass('desactive');
						}
					}, 700);
				});
				$iframe.contents().find('body').on('keyup', function () {
					var titre = $('.h5p-editor-iframe').contents().find('.field-name-extraTitle input').val();
					if (titre !== '') {
						$afficher.removeClass('desactive');
						$enregistrer.removeClass('desactive');
					} else {
						$afficher.addClass('desactive');
						$enregistrer.addClass('desactive');
					}
				});
			});
		}

		function enregistrerContenu (display) {
			if (h5peditor !== undefined) {
				var params = h5peditor.getParams();
                if (params.params !== undefined) {
					$loader.show();
					$.ajax({
						data: JSON.stringify({
							library: h5peditor.getLibrary(),
							params
						}),
						headers: {
							'Content-Type': 'application/json'
						},
						type: 'POST',
						url: '/enregistrer/' + contentId
					}).then((reponse) => {
						if (contentId === 'undefined') {
							const resultat = JSON.parse(reponse);
							if (resultat.contentId) {
								contentId = resultat.contentId;
							}
						}
						if (display === true) {
							lireContenu(contentId);
						} else {
							$loader.hide();
						}
					});
                }
            }
		}

		function lireContenu (id) {
			if (id !== '') {
				$('body').removeClass('editeur');
				$('body').addClass('lecteur');
				$lecteur.html('<iframe class="h5p-player-iframe" src="/lire-iframe/' + id + '" frameborder="0" allowfullscreen="allowfullscreen">');
				$iframe = $('.h5p-player-iframe');
				$iframe.on('load', function () {
					var $head = $iframe.contents().find('head');
					$head.append('<style>.h5p-my-fullscreen-button-enter{display: none;}html.h5p-iframe > body{padding: 2rem;box-sizing: border-box;}</style>');
					$loader.hide();
					$lire.fadeIn();
					$editer.show();
					$afficher.hide();
					$creer.hide();
				});
			} else {
				$loader.hide();
			}
		}
    };

    H5PEditor.getAjaxUrl = function (action, parameters) {
        var url = H5PIntegration.editor.ajaxPath + action;
        if (parameters !== undefined) {
            for (var property in parameters) {
                if (parameters.hasOwnProperty(property)) {
                    url += '&' + property + '=' + parameters[property];
                }
            }
        }
        url += window.location.search.replace(/\?/g, '&');
        return url;
    };

    $(document).ready(H5PEditor.init);
})(H5P.jQuery);
