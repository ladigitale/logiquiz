(function($) {
	var $loader = $('#loader');
	var $liste = $('#liste');
	var $televerser = $('#televerser');
	var $modaleConfirmation = $('#modale-confirmation');
	var $modaleAlerte = $('#modale-alerte');
	var contentId = '';

	$('body').on('keydown', function (event) {
		if (event.key === 'Escape' && $modaleConfirmation.hasClass('ouvert')) {
			$('#annuler').click();
		} else if (event.key === 'Escape' && $modaleAlerte.hasClass('ouvert')) {
			$('#valider').click();
		}
	});

	$('.bouton.primaire.televerser').on('keydown', function (event) {
		if (event.key === 'Enter') {
			$televerser.click();
		}
	});

	$televerser.on('change', function (event) {
		if (event.target.files && event.target.files[0]) {
			$loader.show();
			const fichier = event.target.files[0];
			const formdata = new FormData();
			formdata.append('fichier', fichier);
			$.ajax({
				type: 'POST',
				url: '/televerser',
				data: formdata,
				cache: false,
				contentType: false,
				processData: false
			}).then((reponse) => {
				$televerser.val('');
				const resultat = JSON.parse(reponse);
				if (resultat.contentId) {
					window.location.href = '/lire/' + resultat.contentId;
				} else {
					$loader.hide();
					$modaleAlerte.find('.message').text(resultat.error);
					$modaleAlerte.addClass('ouvert');
				}
			});
		} else {
			$televerser.val('');
		}
	});

	$('.exporter').click(function () {
		contentId = $(this).data('id');
		if (contentId !== '') {
			$loader.show();
			$.ajax({
				type: 'POST',
				url: '/exporter/' + contentId
			}).then((result) => {
				$loader.hide();
				var file = new File([result], contentId + '.html', { type: 'text/plain;charset=utf-8'} );
				saveAs(file);
			});
		}
	});

	$('.supprimer').click(function () {
		$modaleConfirmation.find('.message').text('Souhaitez-vous vraiment supprimer cette ressource ? Cette action est irréversible.');
		$modaleConfirmation.addClass('ouvert');
		contentId = $(this).data('id');
		$modaleConfirmation.find('.bouton').focus();
	});

	$('#annuler').click(function () {
		$modaleConfirmation.removeClass('ouvert');
		$modaleConfirmation.find('.message').text('');
		contentId = '';
	});

	$('#confirmer').click(function () {
		$modaleConfirmation.removeClass('ouvert');
		$modaleConfirmation.find('.message').text('');
		if (contentId !== '') {
			$.ajax({
				type: 'POST',
				url: '/supprimer/' + contentId
			}).then((result) => {
				if (result === 'supprime') {
					$liste.find('.bouton.supprimer[data-id="' + contentId + '"]').closest('.item').remove();
					if ($('#contenu-liste').children().length === 0) {
						$liste.hide();
					}
					contentId = '';
				} else {
					$modaleConfirmation.find('.message').text(result);
					$modaleAlerte.addClass('ouvert');
					contentId = '';
				}
			});
		}
	});

	$('#valider').click(function () {
		$modaleAlerte.removeClass('ouvert');
		$modaleAlerte.find('.message').text('');
	});

	$liste.find('.titre a').click(function () {
		$loader.show();
	});

	$(window).on('load', function () {
		$loader.hide();
		if ($('#contenu-liste').children().length > 0) {
			$liste.show();
		}
		$.getJSON('/version.json', function (data) {
			var version = data.logiquiz;
			var url = data.updateUrl;
			var random = Math.round(Math.random() * 10000);
			$.ajax({
				type: 'GET',
				url: url + 'version.php?rand=' + random
			}).then((result) => {
				if (result && result !== version) {
					$('#credits').prepend('<p><a href="' + url + '#telecharger" target="_blank" style="color: #00ced1;"><b>Une mise à jour de Logiquiz est disponible.</b></a></p>');
				}
			});
		});
	});
})(H5P.jQuery);
