import fs from 'fs-extra'
import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import { fileURLToPath } from 'url'
import cors from 'cors'
import fileUpload from 'express-fileupload'
import H5P from '@lumieducation/h5p-server'
import h5pAjaxExpressRouter from '@lumieducation/h5p-express/build/H5PAjaxRouter/H5PAjaxExpressRouter.js'
import H5PHtmlExporter from '@lumieducation/h5p-html-exporter/build/HtmlExporter.js'
import User from './user.js'
import template from './template.js'
import { app, shell, BrowserWindow, Menu, MenuItem, globalShortcut, systemPreferences, screen } from 'electron'

const server = express()
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

if (app.isPackaged) {
	process.env.NODE_ENV = 'production'
	Menu.setApplicationMenu(false)
}
app.allowRendererProcessReuse = true

let mainWindow, splash
let splashscreen = true

const demarrer = async () => {
	let cheminLibrairies, cheminTemp, cheminContenu, cheminLogiquiz
	const portable = false
	const version = '1.1.1'
	if (process.env.NODE_ENV === 'production' && portable === false) {
		cheminLogiquiz = path.normalize(app.getPath('documents') + '/Logiquiz')
		cheminLibrairies = path.normalize(app.getPath('documents') + '/Logiquiz/librairies')
		cheminContenu = path.normalize(app.getPath('documents') + '/Logiquiz/contenu')
		cheminTemp = path.normalize(app.getPath('documents') + '/Logiquiz/temp')
	} else if (process.env.NODE_ENV === 'production' && portable === true) {
		cheminLibrairies = path.resolve(__dirname, '../librairies')
		cheminContenu = path.resolve(__dirname, '../../contenu')
		cheminTemp = path.resolve(__dirname, '../../temp')
	} else {
		cheminLibrairies = path.resolve(__dirname, './libraries')
		cheminContenu = path.resolve(__dirname, './h5p/content')
		cheminTemp = path.resolve(__dirname, './h5p/temp')
	}

	fs.mkdirpSync(cheminLibrairies)
	fs.mkdirpSync(cheminContenu)
	fs.mkdirpSync(cheminTemp)

	if (process.env.NODE_ENV === 'production' && portable === false) {
		const contenuLibrairies = fs.readdirSync(cheminLibrairies)
		const librairiesTxt = fs.existsSync(cheminLogiquiz + '/librairies.txt')
		if (contenuLibrairies.length > 0) {
			if ((librairiesTxt === true && fs.readFileSync(cheminLogiquiz + '/librairies.txt', 'utf8') !== version) || librairiesTxt === false) {
				fs.copySync(path.resolve(__dirname, '../librairies'), cheminLibrairies)
				fs.writeFileSync(cheminLogiquiz + '/librairies.txt', version)
			}
		} else {
			fs.copySync(path.resolve(__dirname, '../librairies'), cheminLibrairies)
			fs.writeFileSync(cheminLogiquiz + '/librairies.txt', version)
		}
	}

	const config = await new H5P.H5PConfig().load()
	config.uuid = 'f6f86841-1ebd-4de4-a4db-4a016f4e5446'
	config.coreApiVersion = { major: 1, minor: 27 }
	config.h5pVersion = '1.27-master'
	config.maxFileSize = 2147483648
	config.maxTotalSize = 2147483648
    config.platformName = 'Logiquiz'
	config.playerAddons = {
        "H5P.Accordion": ["H5P.MathDisplay"],
        "H5P.AdventCalendar": ["H5P.MathDisplay"],
        "H5P.ArithmeticQuiz": ["H5P.MathDisplay"],
        "H5P.Column": ["H5P.MathDisplay"],
        "H5P.Dialogcards": ["H5P.MathDisplay"],
        "H5P.DocumentationTool": ["H5P.MathDisplay"],
        "H5P.DragQuestion": ["H5P.MathDisplay"],
        "H5P.DragText": ["H5P.MathDisplay"],
        "H5P.Essay": ["H5P.MathDisplay"],
        "H5P.Blanks": ["H5P.MathDisplay"],
        "H5P.Flashcards": ["H5P.MathDisplay"],
        "H5P.InteractiveBook": ["H5P.MathDisplay"],
        "H5P.MarkTheWords": ["H5P.MathDisplay"],
        "H5P.MultipleChoice": ["H5P.MathDisplay"],
        "H5P.QuestionSet": ["H5P.MathDisplay"],
        "H5P.TrueFalse": ["H5P.MathDisplay"],
        "H5P.InteractiveVideo": ["H5P.MathDisplay"],
        "H5P.CoursePresentation": ["H5P.MathDisplay"]
    }
    config.editorAddons = {
        "H5P.Accordion": ["H5P.MathDisplay"],
        "H5P.AdventCalendar": ["H5P.MathDisplay"],
        "H5P.ArithmeticQuiz": ["H5P.MathDisplay"],
        "H5P.Column": ["H5P.MathDisplay"],
        "H5P.Dialogcards": ["H5P.MathDisplay"],
        "H5P.DocumentationTool": ["H5P.MathDisplay"],
        "H5P.DragQuestion": ["H5P.MathDisplay"],
        "H5P.DragText": ["H5P.MathDisplay"],
        "H5P.Essay": ["H5P.MathDisplay"],
        "H5P.Blanks": ["H5P.MathDisplay"],
        "H5P.Flashcards": ["H5P.MathDisplay"],
        "H5P.InteractiveBook": ["H5P.MathDisplay"],
        "H5P.MarkTheWords": ["H5P.MathDisplay"],
        "H5P.MultipleChoice": ["H5P.MathDisplay"],
        "H5P.QuestionSet": ["H5P.MathDisplay"],
        "H5P.TrueFalse": ["H5P.MathDisplay"],
        "H5P.InteractiveVideo": ["H5P.MathDisplay"],
        "H5P.CoursePresentation": ["H5P.MathDisplay"]
    }

	const h5pEditor = H5P.fs(
		config,
		cheminLibrairies,
		cheminTemp,
		cheminContenu
	)

	h5pEditor.temporaryFileManager.cleanUp()
	h5pEditor.contentTypeCache.updateIfNecessary()

	const h5pPlayer = new H5P.H5PPlayer(
		h5pEditor.libraryStorage,
		h5pEditor.contentStorage,
		config
	)

	const htmlExporter = new H5PHtmlExporter.default(
        h5pEditor.libraryStorage,
        h5pEditor.contentStorage,
        h5pEditor.config,
        path.resolve(__dirname, './h5p/core'),
		path.resolve(__dirname, './h5p/editor'),
		template
    )
	
	server.set('trust proxy', 1)
	server.set('views', path.join(__dirname, './views'))
	server.set('view engine', 'pug')
	server.use(cors())
	server.use(express.static(path.join(__dirname, './public')))
	server.use(bodyParser.json({ limit: '2048mb' }))
	server.use(bodyParser.urlencoded({ limit: '2048mb', extended: true }))
	server.use(fileUpload())
	server.use((req, res, next) => {
		req.user = new User()
		next()
	})
	server.use(
		h5pEditor.config.baseUrl,
		h5pAjaxExpressRouter.default(
			h5pEditor,
			path.resolve(__dirname, './h5p/core'),
			path.resolve(__dirname, './h5p/editor'),
			undefined,
			'fr'
		)
	)

	const listen = server.listen()
	const port = listen.address().port

	app.whenReady().then(creerFenetre)

	app.on('window-all-closed', () => {
		if (process.platform !== 'darwin') {
			app.quit()
		}
	})

	app.on('activate', () => {
		if (BrowserWindow.getAllWindows().length === 0) {
			creerFenetre()
		}
	})

	server.get('/', async (req, res) => {
		const contentIds = await h5pEditor.contentManager.listContent()
		const contentObjects = await Promise.all(
			contentIds.map(async (id) => ({
				content: await h5pEditor.contentManager.getContentMetadata(
					id,
					req.user
				),
				id
			}))
		)
		let liste = ''
		if (contentObjects.length > 0) {
			liste += '<h2>Mes contenus interactifs</h2><hr><div id="contenu-liste">'
			liste += contentObjects.map((content) => '<div class="item"><div class="titre"><a href="/lire/' + content.id + '"><h5>' + content.content.title + '</h5></a><div class="petit"><div class="meta"><span class="material-icons">info</span>' + content.content.mainLibrary + '</div></div></div><div class="actions"><a class="bouton editer" href="/editer/' + content.id + '" title="Éditer"><span class="material-icons">edit</span></a><a class="bouton telecharger" href="' + h5pEditor.config.baseUrl + h5pEditor.config.downloadUrl + '/' + content.id + '" donwload="' + content.id + '.h5p" title="Télécharger au format H5P"><span class="material-icons">get_app</span></a><button class="bouton exporter" data-id="' + content.id + '" title="Exporter en HTML"><span class="material-icons">web</span></button><button class="bouton supprimer" data-id="' + content.id + '" title="Supprimer"><span class="material-icons">delete</span></button></div></div>').join('')
			liste += '</div>'
		}
		res.render('accueil', { liste: liste })
		res.end()
	})
	
	server.get('/creer', async (req, res) => {
		h5pEditor.setRenderer(model => genererEditeur (model))
		const result = await h5pEditor.render(undefined, 'fr', req.user)
		const data = await fs.readJson(path.join(__dirname, './public/h5p-editor-template.json'))
		result.integration.editor.copyrightSemantics = data.copyrightSemantics
		result.integration.editor.metadataSemantics = data.metadataSemantics
		result.integration.l10n = data.l10n
		res.render('editeur', { scripts: result.scripts, integration: 'window.H5PIntegration = parent.H5PIntegration || ' + JSON.stringify(result.integration) })
		res.end()
	})

	server.get('/lire-iframe/:contentId', async (req, res) => {
		try {
			h5pPlayer.setRenderer(model => genererLecteur (model))
			const result = await h5pPlayer.render(req.params.contentId, 'fr', req.user)
			const data = await fs.readJson(path.join(__dirname, './public/h5p-player-template.json'))
			result.integration.l10n = data.l10n
			res.render('lecteur-iframe', { scripts: result.scripts, styles: result.styles, contentId: result.contentId, integration: 'H5PIntegration = ' + JSON.stringify(result.integration) })
			res.end()
		} catch (error) {
			res.status(500).end()
		}
	})
	
	server.get('/lire/:contentId', async (req, res) => {
		try {
			h5pPlayer.setRenderer(model => genererLecteur (model))
			const result = await h5pPlayer.render(req.params.contentId, 'fr', req.user)
			const data = await fs.readJson(path.join(__dirname, './public/h5p-player-template.json'))
			result.integration.l10n = data.l10n
			res.render('lecteur', { scripts: result.scripts, styles: result.styles, contentId: result.contentId, integration: 'H5PIntegration = ' + JSON.stringify(result.integration) })
			res.end()
		} catch (error) {
			res.status(500).end()
		}
	})

	server.get('/editer/:contentId', async (req, res) => {
		h5pEditor.setRenderer(model => genererEditeur (model))
		const result = await h5pEditor.render(req.params.contentId, 'fr', req.user)
		const data = await fs.readJson(path.join(__dirname, './public/h5p-editor-template.json'))
		result.integration.editor.copyrightSemantics = data.copyrightSemantics
		result.integration.editor.metadataSemantics = data.metadataSemantics
		result.integration.l10n = data.l10n
		res.render('editeur', { scripts: result.scripts, integration: 'window.H5PIntegration = parent.H5PIntegration || ' + JSON.stringify(result.integration) })
		res.end()
	})

	server.post('/exporter/:contentId', async (req, res) => {
        const html = await htmlExporter.createSingleBundle(
            req.params.contentId,
            req.user
        )
        res.send(html)
    })

	server.post('/enregistrer/:contentId', async (req, res) => {
		let contentId
		if (req.params.contentId.toString() === 'undefined') {
			contentId = await h5pEditor.saveOrUpdateContent(
				undefined,
				req.body.params.params,
				req.body.params.metadata,
				req.body.library,
				req.user
			)
		} else {
			contentId = await h5pEditor.saveOrUpdateContent(
				req.params.contentId.toString(),
				req.body.params.params,
				req.body.params.metadata,
				req.body.library,
				req.user
			)
		}
		res.send(JSON.stringify({ contentId }))
	})

	server.post('/supprimer/:contentId', async (req, res) => {
		try {
			await h5pEditor.deleteContent(req.params.contentId, req.user)
			res.send('supprime')
		} catch (error) {
			res.send('Erreur lors de la suppression du contenu.')
		}
	})

	server.post('/h5p/contentUserData', (req, res) => {
		res.status(200).send()
	})

	server.get('/h5p/contentUserData', (req, res) => {
        res.status(200).json({})
	})

    server.post('/setFinished', (req, res) => {
		res.status(200).send()
	})

	server.post('/televerser', async (req, res) => {
		const buffer = req.files.fichier.data
		const result = await h5pEditor.uploadPackage(buffer, req.user)
		const contentId = await h5pEditor.saveOrUpdateContent(
			undefined,
			result.parameters,
			result.metadata,
			recupererLibrairie(result.metadata),
			req.user
		) 
		fs.emptyDirSync(cheminTemp)
		res.send(JSON.stringify({ contentId }))
	})
	
	function genererEditeur (model) {
		const data = {}
		data.scripts = model.scripts
		data.styles = model.styles
		data.integration = model.integration
		return data
	}

	function genererLecteur (model) {
		const data = {}
		data.scripts = model.scripts
		data.styles = model.styles
		data.integration = model.integration
		data.contentId = model.contentId
		data.customScripts = model.customScripts
		return data
	}

	function recupererLibrairie (metadata) {
		const library = (metadata.preloadedDependencies || []).find(
			dependency => dependency.machineName === metadata.mainLibrary
		)
		if (!library) {
			return ''
		}
		return H5P.LibraryName.toUberName(library, { useWhitespace: true })
	}

	function creerFenetre () {
		const dimensions = screen.getPrimaryDisplay().size
		let largeur = dimensions.width - 400
		let hauteur = dimensions.height - 150
		if (largeur < 800) {
			largeur = 800
		}
		if (hauteur < 600) {
			hauteur = 600
		}

		const cheminIcone = path.join(`${__dirname}/public/img/logiquiz.png`)
		mainWindow = new BrowserWindow({
			minWidth: 800,
			minHeight: 600,
			width: largeur,
			height: hauteur,
			center: true,
			icon: cheminIcone,
			webPreferences: {
				nativeWindowOpen: true,
				contextIsolation: true,
				sandbox: false
			}
		})
		mainWindow.loadURL('http://localhost:' + port + '/')

		if (splashscreen !== false) {
			splash = new BrowserWindow({
				parent: mainWindow,
				width: 350,
				height: 350,
				transparent: true,
				center: true,
				frame: false,
				alwaysOnTop: true,
				icon: cheminIcone,
				webPreferences: {
					nativeWindowOpen: true,
					contextIsolation: true,
					sandbox: false
				}
			})
			splash.loadURL(`file://${__dirname}/public/splash.html`)
			splash.show()
			mainWindow.show()
		} else {
			mainWindow.show()
			mainWindow.focus()
		}

		setTimeout(() => {
			if (splashscreen === true) {
				splash.close()
			}
			splashscreen = false
			mainWindow.focus()
		}, 1500)

		mainWindow.webContents.setWindowOpenHandler(({ url }) => {
			shell.openExternal(url)
			return { action: 'deny' }
		})

		mainWindow.webContents.on('context-menu', (_, props) => {
			const menu = new Menu()
			if (props.isEditable) {
				menu.append(new MenuItem({ label: 'Couper', role: 'cut' }))
				menu.append(new MenuItem({ label: 'Copier', role: 'copy' }))
				menu.append(new MenuItem({ label: 'Coller', role: 'paste' }))
				menu.popup()
			}
		})

		if (process.platform === 'darwin') {
			mainWindow.on('focus', () => {
				if (mainWindow.isFocused()) {
					globalShortcut.register('CommandOrControl+C', () => {
						mainWindow.webContents.copy()
					})

					globalShortcut.register('CommandOrControl+V', () => {
						mainWindow.webContents.paste()
					})

					globalShortcut.register('CommandOrControl+X', () => {
						mainWindow.webContents.cut()
					})

					globalShortcut.register('CommandOrControl+A', () => {
						mainWindow.webContents.selectAll()
					})
				}
			})

			mainWindow.on('blur', () => {
				globalShortcut.unregister('CommandOrControl+C')
				globalShortcut.unregister('CommandOrControl+V')
				globalShortcut.unregister('CommandOrControl+X')
			})

			systemPreferences.askForMediaAccess('microphone').then(function (granted) {
				if (!granted) {
					alert('Vous ne pourrez pas enregistrer votre voix si l\'accès au microphone n\'est pas autorisé.')
				}
			})
		}
	}
}

demarrer()
